# Tidy-Tuesdays

Viz and other fun! But mostly viz.

## March 2019

This month I want to highlight a book I was recently gifted. If you haven't read _W.E.B. Du Bois's Data Portraits: Visualizing Black America_, go [buy](https://www.amazon.com/W-Boiss-Data-Portraits-Visualizing/dp/1616897066) this book!

_W.E.B. Du Bois's Data Portraits: Visualizing Black America_ contains a collection of infographic plates originally created for the 1900 American Negro Exhibit in Paris by none other than W.E.B.  Du Bois and his team of researchers based at Atlanta University. These visualizations stood as infographic activism to provide accurate illustrations of what Du Bois called, "the color line." The original exhibition aimed to illustrate progress made by African Americans since Emancipation.

During March, I'll try my hand at recreating some the visualizations from the book in R! I've primarily used [plotly](https://plot.ly/r/) (which I _love_) in the past for visualization purposes, but I after watching videos during rstudioconf2019, I wanted to explore more of the flexibility found in ggplot! What better way to learn ggplot(and add-ons) and celebrate another accomplishment by polymath, W.E.B Du Bois.

:raised_hands: Thankfully, I am able to stand on the shoulders of giants:

- General shape and structure: [ggplot2](https://ggplot2.tidyverse.org/)
- Background and coloring: [ggpomological](https://www.garrickadenbuie.com/project/ggpomological/) The release timing for this package couldn't have been more perfect! The pomological watercolors and paper background closely mimic the gouache used in the original plates. Thanks, @[grrrck](https://twitter.com/grrrck)!
- Additional resource: _Data Visualization_ by Kieran Healy

Viz:
![pop_increaseof.png](webdata_portraits/visualizations/pop_increaseof.png)

Rmarkdown Document:
![markdown.png](webdata_portraits/visualizations/markdown.png)

[Code Here](webdata_portraits/code/W.E.B.dataportraits.Rmd)
